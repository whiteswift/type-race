# Type Race

Type the words as fast as you can. Snooze you lose!

This is a simple typing game to demo react hooks

![Type race demo](public/demo.png)

# Demo
[https://type-race.whitedarryl101.vercel.app/](https://type-race.whitedarryl101.vercel.app/)
