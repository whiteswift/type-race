import React, { useState } from 'react';
import useFocus from './hooks/useFocus';

function App() {
  const INITIAL_GAME_STATE = { win: false, startTime: null, endTime: null }
  const [prompt, setPrompt] = useState('Choose a snippet 👇')
  const [snippet, setSnippet] = useState('')
  const [userText, setUserText] = useState('')
  const [gameState, setGameState] = useState(INITIAL_GAME_STATE)
  const [inputRef, setInputFocus] = useFocus();
  const [statusColour, setstatusColour] = useState('neutral')
  const snippetList = [ // Import a dictionary? Get user to choose maybe words from languages?
    'The brown fox',
    'Yeah',
    'What year is it?',
    'Bears, bees, star wars and peas',
    'dark chocolate and raisins',
    '2x vodka, 1x midori, 1x blue curacao, red bull',
    'Ryunosuke Akutagawa Hell Screen, penguin modern classics'
  ];

  const chooseSnippet = (snippetIndex) => () => {
    clearProgress()
    setPrompt('')
    setInputFocus(inputRef)
    setSnippet(snippetList[snippetIndex])
    setGameState({ ...gameState, win: false, startTime: new Date().getTime() })
  }

  const updateUserText = (event) => {
    setUserText(event.target.value)

    snippet.startsWith(userText) ? setstatusColour('good-green') : setstatusColour('bad-red')

    if (event.target.value === snippet) {
      gameWon()
    }
  }

  const gameWon = () => {
    setGameState({
      ...gameState,
      win: true,
      endTime: (new Date().getTime() - gameState.startTime) / 1000
    })
  }

  const clearProgress = () => {
    setUserText('')
    setstatusColour('neutral')
  }

  return (
    <div className="App">
      <header className="App-header" >
        <h1>Typing Race </h1>
        <p>{prompt}</p>
        <div className='progress'>
          <div id='activeSnippet' className={statusColour}>{snippet}</div>
          <h4 id='winStatus' className={gameState.win ? 'text-pop-up' : null}>
            {gameState.win ? `Done! 🎉 Time: ${gameState.endTime} seconds` : null}
          </h4>
        </div>
      </header>
      <input ref={inputRef} id="snippetInput" type='text' value={userText} onChange={updateUserText} />
      <hr />
      {
        snippetList.map((snippetItem, index) => (
          <button onClick={chooseSnippet(index)} key={index} className={'snippet--item'}>
            {snippetItem.substring(0, 10)}
          </button>
        ))
      }
    </div>
  )
}

export default App;
